<?php


namespace SFExpressIsp\Services;


use SFExpressIsp\Core\ItemAbstract;
use SFExpressIsp\Core\ServiceInterface;

class OrderService extends ItemAbstract implements ServiceInterface
{
    protected $objTypes = ['AdditionalDataList', 'ShipFromAddress', 'ShipToAddress', 'OtherAddress', 'Parcels', 'TradeOrder', 'CustomsItem', 'AddedService', 'OrderResponse'];

    protected $arrayTypes = ['LineItem'];

    public $data = [
        'OrderNo' => '',
        'ExpressServiceCode' => 208,
        'IsGenMailNo' => 1,
        'ParcelQuantity' => 1,
        'TotalWeight' => '',
        'PaymentMethod' => 1,
        'PaymentType' => 1,
        'PaymentAccountNo' => '',
        'IsDoCall' => 0,
        'NeedReturnTrackingNo' => 0,
        'PickupMethod' => 1,
        'DeliveryMethod' => 1,
        'AdditionalDataList' => null,
        'ShipFromAddress' => null,
        'ShipToAddress' => null,
        'OtherAddress' => null,
        'Parcels' => null,
        'LineItem' => null,
        'TradeOrder' => null,
        'CustomsItem' => null,
        'AddedService' => null,
        'OrderResponse' => null,
    ];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * @return string
     * @author luffyzhao@vip.126.com
     */
    public function __toString(): string
    {
        $string = parent::__toString();
        return sprintf('<OrderList><Order>%s</Order></OrderList>', $string);
    }
}