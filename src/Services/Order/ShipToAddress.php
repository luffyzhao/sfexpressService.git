<?php


namespace SFExpressIsp\Services\Order;


use SFExpressIsp\Core\ItemAbstract;

class ShipToAddress extends ItemAbstract
{

    protected $objTypes = ['AdditionalDataList'];

    protected $data = [
        'CompanyName' => '',
        'Contact' => '',
        'SFStoreCode' => '',
        'Telephone' => '',
        'Mobile' => '',
        'Email' => '',
        'CountryCode' => 'CN',
        'StateOrProvince' => '',
        'City' => '',
        'County' => '',
        'AddressLine1' => '',
        'AddressLine2' => '',
        'DeliveryCode' => '',
        'PostalCode' => '',
        'ContactCN' => '',
        'AdditionalDataList' => null,
    ];
}